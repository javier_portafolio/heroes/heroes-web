export const environment = {
  production: true,
  title: 'Heroes App',
  api_url: 'https://heroes-app-api.herokuapp.com/api',
};
