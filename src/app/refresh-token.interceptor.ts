import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { AuthService } from './shared/services/auth.service';
import { Response } from './shared/models/response';
import { Router } from '@angular/router';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  // for avoiding entering an infinite loop
  private isRefreshing = false;

  constructor(private authService: AuthService, private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.error.code === 401) {
          return this.handleAuthorizationError(request, next);
        }
        else if (error instanceof HttpErrorResponse && error.error.message == "Token Signature verification failed" || error.error.message == "El refresh token ha expirado, inicie sesión nuevamente.") {
          localStorage.clear();
          this.router.navigateByUrl('login');
          return next.handle(request);
        }
        else {
          return throwError(error);
        }
      })
    );
  }

  private setToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
  }

  private handleAuthorizationError(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      return this.authService.tokenRefresh().pipe(
        switchMap((response: Response) => {
          this.isRefreshing = false;
          localStorage.setItem('token', response.body['token']);
          localStorage.setItem('refresh_token', response.body['refresh_token']);

          return next.handle(this.setToken(request, response.body['token']));
        })
      );
    } else {
      return next.handle(request);
    }
  }
}
