import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContentHeaderComponent } from './components/content-header/content-header.component';
import { RouterModule } from '@angular/router';
import { PreloaderComponent } from './components/preloader/preloader.component';



@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ContentHeaderComponent,
    PreloaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ContentHeaderComponent,
    PreloaderComponent
  ]
})
export class SharedModule { }
