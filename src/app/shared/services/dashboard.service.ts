import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Response } from '../models/response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private url: string = environment.api_url;
  private modulo: string = "/v1/dashboard";
  private headers;

  constructor(private http: HttpClient) { }

  public getData (): Observable<any> {
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "apikey": usuario['apikey']
    }
    return this.http.get<Response>(`${this.url}${this.modulo}`, {
      headers: headers
    });
  }
}
