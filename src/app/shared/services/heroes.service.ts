import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Response } from '../models/response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private url: string = environment.api_url;
  private modulo: string = "/v1/heroes";
  private headers;

  constructor(private http: HttpClient) {
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    this.headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "apikey": usuario['apikey']
    };
  }

  // Obtener lista de heroes
  getHeroes (): Observable<any> {
    return this.http.get<Response>(`${this.url}${this.modulo}`, {
      headers: this.headers
    });
  }

  // Obtener detalle de heroe
  getHeroe (codigo: string): Observable<any> {
    return this.http.get<Response>(`${this.url}${this.modulo}/${codigo}`, {
      headers: this.headers
    });
  }

  // Crear heroe
  setHeroe (body: any): Observable<any> {
    return this.http.post<Response>(`${this.url}${this.modulo}`, body, {
      headers: this.headers
    });
  }

  // Obtener data de heroe para editar
  getHeroeEdit (codigo: string): Observable<any> {
    return this.http.get<Response>(`${this.url}${this.modulo}/${codigo}/edit`, {
      headers: this.headers
    });
  }

  // Modificar heroe
  putHeroe (codigo: string, body: any): Observable<any> {
    return this.http.post<Response>(`${this.url}${this.modulo}/${codigo}`, body, {
      headers: this.headers
    });
  }

  deleteHeroe (codigo: string, body: any): Observable<any> {
    return this.http.post<Response>(`${this.url}${this.modulo}/${codigo}`, body, {
      headers: this.headers
    });
  }

  // Obtener generos
  getGeneros (): Observable<any> {
    return this.http.get<Response>(`${this.url}/v1/referentials/genders`, {
      headers: this.headers
    });
  }

  // Obtener ocupaciones
  getOcupaciones (): Observable<any> {
    return this.http.get<Response>(`${this.url}/v1/referentials/occupations`, {
      headers: this.headers
    })
  }
}
