import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Response } from '../models/response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url: string = environment.api_url;
  private modulo: string = "/auth/";

  constructor(private http: HttpClient) { }

  // Logueo en la aplicacion
  public login (body: any): Observable<any> {
    let headers = {
      "Accept": "application/json"
    };
    return this.http.post<Response>(`${this.url}${this.modulo}login`, body, {
      headers: headers
    });
  }

  // Verificar el usuario logueado
  public verifyLogin (body: any): Observable<any> {
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`
    };
    return this.http.post<Response>(`${this.url}${this.modulo}verify`, body, {
      headers: headers
    });
  }

  // Registrar usuario
  public register (body: any): Observable<Response> {
    let headers = {
      "Accept": "application/json"
    };
    return this.http.post<Response>(`${this.url}${this.modulo}register`, body, {
      headers: headers
    });
  }

  // Enviar codigo de recuperacion
  public forget (body: any): Observable<Response> {
    let headers = {
      "Accept": "application/json"
    };
    return this.http.post<Response>(`${this.url}${this.modulo}recovery/send`, body, {
      headers: headers
    });
  }

  // Recuperacion de contraseña
  public recover (body: any): Observable<Response> {
    let headers = {
      "Accept": "application/json"
    };
    return this.http.post<Response>(`${this.url}${this.modulo}recovery`, body, {
      headers: headers
    });
  }

  // Enviar codigo de confirmacion
  public sendCodigo (): Observable<Response> {
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "apikey": usuario['apikey']
    };
    return this.http.get<Response>(`${this.url}${this.modulo}confirmation`, {
      headers: headers
    });
  }

  // Confirmar correo
  public sendConfirmacion (body: any): Observable<Response> {
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "apikey": usuario['apikey']
    };
    return this.http.post<Response>(`${this.url}${this.modulo}confirmation`, body, {
      headers: headers
    });
  }

  // Verificar si el token sigue activo
  public tokenVerify (): Observable<Response> {
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`
    };
    return this.http.get<Response>(`${this.url}${this.modulo}token_validate`, {
      headers: headers
    });
  }

  // Refrescar token
  public tokenRefresh (): Observable<Response> {
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    let headers = {
      "Accept": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "apikey": usuario['apikey']
    };
    let body = new FormData();
    body.append('refresh_token', localStorage.getItem('refresh_token'));
    return this.http.post<Response>(`${this.url}${this.modulo}refresh`, body, {
      headers: headers
    });
  }
}
