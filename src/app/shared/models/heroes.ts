export interface Heroes {
  codigo: string,
  nombre: string,
  alias: string,
  genero: string,
  ocupacion: string,
  especialidad: string,
  fecha_creado: string,
  user_creado: string,
  fecha_modificado: string|null,
  usuario_modifica: string|null
}
