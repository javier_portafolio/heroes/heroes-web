export interface Response {
  code: number,
  error: boolean,
  body: object,
  message: string
}
