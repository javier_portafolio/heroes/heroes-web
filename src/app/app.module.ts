import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { LoginComponent } from './components/pages/auth/login/login.component';
import { RegisterComponent } from './components/pages/auth/register/register.component';
import { HeroesListComponent } from './components/pages/heroes/heroes-list/heroes-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { HeroesDetailComponent } from './components/pages/heroes/heroes-detail/heroes-detail.component';
import { HeroesCreateComponent } from './components/pages/heroes/heroes-create/heroes-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeroesEditComponent } from './components/pages/heroes/heroes-edit/heroes-edit.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ForgetComponent } from './components/pages/auth/forget/forget.component';
import { RecoveryComponent } from './components/pages/auth/recovery/recovery.component';
import { ConfirmationComponent } from './components/pages/auth/confirmation/confirmation.component';
import { NotFoundComponent } from './components/pages/error/not-found/not-found.component';
import { RefreshTokenInterceptor } from './refresh-token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    HeroesListComponent,
    HeroesDetailComponent,
    HeroesCreateComponent,
    HeroesEditComponent,
    ForgetComponent,
    RecoveryComponent,
    ConfirmationComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
