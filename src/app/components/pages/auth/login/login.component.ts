import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../shared/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = environment.title;
  public loading = false;
  public formLogin;
  public errorMessage: string;
  public errores = [];
  public Toast;

  constructor(private renderer: Renderer2, private formBuilder: FormBuilder, private servicio: AuthService, private router: Router) {
    this.renderer.addClass(document.body, 'login-page');
    this.renderer.removeStyle(document.body, 'height');

    this.formLogin = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    if (localStorage.getItem('is_loggedin')) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'login-page');
  }

  private limpiarErrorMessage () {
    setTimeout(() => {
      this.errorMessage = "";
    }, 6000);
  }

  public hasError = (controlName: string, errorName?: string) => {
    if (errorName) {
      return this.formLogin.controls[controlName].hasError(errorName);
    }

    return (this.errores[controlName]) ? true : false;
  }

  public limpiarErrores (name: string) {
    delete this.errores[name];
  }

  public onSubmit () {
    this.loading = true;
    let datos = new FormData();
    datos.append('email', this.formLogin.get('email').value);
    datos.append('password', this.formLogin.get('password').value);

    this.servicio.login(datos)
      .subscribe(result => {
        localStorage.setItem('token', result.body.token);
        localStorage.setItem('refresh_token', result.body.refresh_token);

        this.servicio.verifyLogin(datos)
          .subscribe(result => {
            this.loading = false;
            this.Toast.fire({
              icon: 'success',
              title: result.message,
            });
            localStorage.setItem('usuario', JSON.stringify(result.body.usuario));
            localStorage.setItem('is_loggedin', 'true');
            setTimeout(() => {
              this.router.navigateByUrl('/dashboard');
            }, 2000);
          },
          err => {
            this.loading = false;
            localStorage.clear();
            this.errorMessage = err.error.message;
            this.errores = err.error.body?.errores ?? [];
            this.limpiarErrorMessage();
          })
      },
      err => {
        this.loading = false;
        localStorage.clear();
        this.errorMessage = err.error.message;
        this.errores = err.error.body?.errores ?? [];
        this.limpiarErrorMessage();
      })
  }

}
