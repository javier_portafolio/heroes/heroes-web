import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../shared/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.css']
})
export class RecoveryComponent implements OnInit {

  title = environment.title;
  public loading = false;
  public formRecuperar;
  public errorMessage: string;
  public errores = [];
  public Toast;

  constructor(private renderer: Renderer2, private formBuilder: FormBuilder, private servicio: AuthService, private router: Router) {
    this.renderer.addClass(document.body, 'login-page');
    this.renderer.removeStyle(document.body, 'height');

    this.formRecuperar = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      codigo: ['', Validators.compose([
        Validators.required
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])],
      password_confirmation: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    if (localStorage.getItem('is_loggedin')) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'login-page');
  }

  private limpiarErrorMessage () {
    setTimeout(() => {
      this.errorMessage = "";
    }, 6000);
  }

  public hasError = (controlName: string, errorName?: string) => {
    if (errorName) {
      return this.formRecuperar.controls[controlName].hasError(errorName);
    }

    return (this.errores[controlName]) ? true : false;
  }

  public limpiarErrores (name: string) {
    delete this.errores[name];
  }

  public onSubmit () {
    this.loading = true;
    let datos = new FormData();
    datos.append('email', this.formRecuperar.get('email').value);
    datos.append('codigo', this.formRecuperar.get('codigo').value);
    datos.append('password', this.formRecuperar.get('password').value);
    datos.append('password_confirmation', this.formRecuperar.get('password_confirmation').value);

    this.servicio.recover(datos)
      .subscribe(result => {
        this.loading = false;
        this.errores = [];
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        setTimeout(() => {
          this.router.navigateByUrl('/login');
        }, 2000);
      },
      err => {
        this.loading = false;
        this.errorMessage = err.error.message;
        this.errores = err.error.body?.errores ?? [];
        this.limpiarErrorMessage();

      })
  }

}
