import { Component, OnInit, Renderer2 } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../shared/services/auth.service';
import Swal from 'sweetalert2';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  title = environment.title;
  public loading = false;
  public formRegister;
  public errorMessage: string;
  public errores = [];
  public Toast;

  constructor(private renderer: Renderer2, private formBuilder: FormBuilder, private servicio: AuthService, private router: Router) {
    this.renderer.addClass(document.body, 'register-page');
    this.renderer.removeStyle(document.body, 'height');

    this.formRegister = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])],
      password_confirmation: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    if (localStorage.getItem('is_loggedin')) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'register-page');
  }

  private limpiarErrorMessage () {
    setTimeout(() => {
      this.errorMessage = "";
    }, 6000);
  }

  public hasError = (controlName: string, errorName?: string) => {
    if (errorName) {
      return this.formRegister.controls[controlName].hasError(errorName);
    }

    return (this.errores[controlName]) ? true : false;
  }

  public limpiarErrores (name: string) {
    delete this.errores[name];
  }

  public onSubmit () {
    this.loading = true;
    let datos = new FormData();
    datos.append('name', this.formRegister.get('name').value);
    datos.append('email', this.formRegister.get('email').value);
    datos.append('password', this.formRegister.get('password').value);
    datos.append('password_confirmation', this.formRegister.get('password_confirmation').value);

    this.servicio.register(datos)
      .subscribe(result => {
        this.loading = false;
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        this.router.navigateByUrl('/login');
      },
      err => {
        this.loading = false;
        this.errorMessage = err.error.message;
        this.errores = err.error.body?.errores ?? [];
        this.limpiarErrorMessage();
      })
  }

}
