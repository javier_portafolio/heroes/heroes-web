import { Component, OnInit, Output, Renderer2 } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../shared/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  @Output() headers = {
    "title": "Confirmacion de correo",
    "bread": [
      {
        "title": "Tablero",
        "href": "dashboard"
      },
      {
        "title": "Confirmacion",
      }
    ]
  };
  private sidebar_open: boolean;
  public Toast;
  public formConfirmacion;
  public preloader = true;
  public confirmacion = false;
  public errorMessage: string;
  public errores = [];
  public loading = false;

  constructor(private renderer: Renderer2, private formBuilder: FormBuilder, private servicio: AuthService, private router: Router) {
    this.sidebar_open = document.body.classList.contains('sidebar-open');
    this.renderer.addClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');

    if (this.sidebar_open) {
      this.renderer.addClass(document.body, 'sidebar-closed');
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }

    this.formConfirmacion = this.formBuilder.group({
      codigo: ['', Validators.compose([
        Validators.required
      ])],
    });

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    this.ocultarPreloader();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');
  }

  private limpiarErrorMessage () {
    setTimeout(() => {
      this.errorMessage = "";
    }, 6000);
  }

  private ocultarPreloader () {
    setTimeout(() => {
      this.preloader = false;
    }, 1000);
  }

  public hasError = (controlName: string, errorName?: string) => {
    if (errorName) {
      return this.formConfirmacion.controls[controlName].hasError(errorName);
    }

    return (this.errores[controlName]) ? true : false;
  }

  public limpiarErrores (name: string) {
    delete this.errores[name];
  }

  public onSend () {
    this.confirmacion = true;

    this.servicio.sendCodigo()
      .subscribe(result => {
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
      },
      err => {
        this.Toast.fire({
          icon: 'error',
          title: err.error.message ?? "Error al enviar codigo.",
        });
      })

    setTimeout(() => {
      this.confirmacion = false;
    }, 2000);
  }

  public onConfirmar () {
    this.loading = true;
    let datos = new FormData();
    datos.append('codigo', this.formConfirmacion.get('codigo').value);

    this.servicio.sendConfirmacion(datos)
      .subscribe(result => {
        this.loading = false;
        this.errores = [];
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        let usuario = JSON.parse(localStorage.getItem('usuario'));
        usuario['confirmado'] = true;
        localStorage.setItem('usuario', JSON.stringify(usuario));
        this.router.navigateByUrl('/dashboard');
      },
      err => {
        this.loading = false;
        this.Toast.fire({
          icon: 'error',
          title: err.error.message ?? "Error al enviar codigo.",
        });
        this.errorMessage = err.error.message;
        this.errores = err.error.body?.errores ?? [];
        this.limpiarErrorMessage();
      })
  }

}
