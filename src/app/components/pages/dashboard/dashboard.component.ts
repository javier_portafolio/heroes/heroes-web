import { Component, OnInit, Output, Renderer2 } from '@angular/core';
import { DashboardService } from '../../../shared/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Output() headers = {
    "title": "Tablero",
    "bread": [
      {
        "title": "Tablero"
      }
    ]
  };
  private sidebar_open: boolean;
  public Toast;
  public total = 0;
  public totalUsuario = 0;

  constructor(private renderer: Renderer2, private servicio: DashboardService) {
    this.sidebar_open = document.body.classList.contains('sidebar-open');
    this.renderer.addClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');

    if (this.sidebar_open) {
      this.renderer.addClass(document.body, 'sidebar-closed');
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    this.datos();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'sidebar-mini');
    this.renderer.removeClass(document.body, 'layout-fixed');
  }

  public datos () {
    this.servicio.getData()
      .subscribe(result => {
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        this.total = result.body?.dashboard.total || 0;
        this.totalUsuario = result.body?.dashboard.totalUsuario || 0;
        console.log(result);
      },
      err => {
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

}
