import { Component, OnInit, Output, Renderer2 } from '@angular/core';
import { HeroesService } from '../../../../shared/services/heroes.service';
import Swal from 'sweetalert2';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-heroes-edit',
  templateUrl: './heroes-edit.component.html',
  styleUrls: ['./heroes-edit.component.css']
})
export class HeroesEditComponent implements OnInit {

  @Output() headers = {
    "title": "Heroes",
    "bread": [
      {
        "title": "Tablero",
        "href": "dashboard"
      },
      {
        "title": "Heroes",
        "href": "heroes"
      },
      {
        "title": "Heroe",
        "href": "/heroes/:codigo"
      },
      {
        "title": "Edit"
      }
    ]
  };
  @Output() modulo = "heroes";
  private sidebar_open: boolean;
  public Toast;
  public codigo: string;
  public formHeroe;
  public generosLista = [];
  public ocupacionesLista = [];
  public errores = [];
  public preloader = true;

  constructor(private renderer: Renderer2, private actRoute: ActivatedRoute, private formBuilder: FormBuilder, private servicio: HeroesService, private router: Router) {
    this.sidebar_open = document.body.classList.contains('sidebar-open');
    this.renderer.addClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');

    if (this.sidebar_open) {
      this.renderer.addClass(document.body, 'sidebar-closed');
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    this.formHeroe = this.formBuilder.group({
      nombre: ['', Validators.compose([
        Validators.required
      ])],
      alias: ['', Validators.compose([
        Validators.required
      ])],
      genero: ['', Validators.compose([
        Validators.required
      ])],
      ocupacion: ['', Validators.compose([
        Validators.required
      ])],
      especialidad: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.codigo = this.actRoute.snapshot.params.codigo;
    this.headers.bread[2].href = this.headers.bread[2].href.replace(':codigo', this.codigo);
    this.ocultarPreloader();
  }

  ngOnInit(): void {
    this.generos();
    this.ocupaciones();
    this.heroeEdit();
  }

  private ocultarPreloader () {
    setTimeout(() => {
      this.preloader = false;
    }, 1000);
  }

  private generos () {
    this.servicio.getGeneros()
      .subscribe(result => {
        this.generosLista = result.body.generos;
      },
      err => {
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

  private ocupaciones () {
    this.servicio.getOcupaciones()
      .subscribe(result => {
        this.ocupacionesLista = result.body.ocupaciones;
      },
      err => {
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

  private heroeEdit () {
    this.preloader = true;
    this.servicio.getHeroeEdit(this.codigo)
      .subscribe(result => {
        this.ocultarPreloader();
        let heroe = result.body.heroe;
        let datos = {
          nombre: heroe.nombre,
          alias: heroe.alias,
          genero: heroe.genero_id,
          ocupacion: heroe.ocupacion_id,
          especialidad: heroe.especialidad
        }
        this.headers.bread[2].title = heroe.nombre;
        this.formHeroe.patchValue(datos);
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
        if (err.error.code == 403) {
          setTimeout(() => {
            this.router.navigateByUrl('heroes');
          }, 2000);
        }
      })
  }

  public hasError = (controlName: string, errorName?: string) => {
    if (errorName) {
      return this.formHeroe.controls[controlName].hasError(errorName);
    }

    return (this.errores[controlName]) ? true : false;
  }

  public limpiarErrores (name: string) {
    delete this.errores[name];
  }

  public onSubmit () {
    this.preloader = true;
    let datos = new FormData();
    datos.append('_method', 'PUT');
    datos.append('nombre', this.formHeroe.get('nombre').value);
    datos.append('alias', this.formHeroe.get('alias').value);
    datos.append('genero', this.formHeroe.get('genero').value);
    datos.append('ocupacion', this.formHeroe.get('ocupacion').value);
    datos.append('especialidad', this.formHeroe.get('especialidad').value);

    this.servicio.putHeroe(this.codigo, datos)
      .subscribe(result => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: (result.message.includes('no')) ? 'info' : 'success',
          title: result.message,
        });
        setTimeout(() => {
          this.router.navigateByUrl(`/heroes/${this.codigo}`);
        }, 2000);
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
        if (err.error.body?.errores) {
          this.errores = err.error.body.errores;
        }
      })
  }

}
