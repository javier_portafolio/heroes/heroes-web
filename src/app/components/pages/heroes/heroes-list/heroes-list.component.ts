import { Component, OnInit, Output, Renderer2 } from '@angular/core';
import { HeroesService } from '../../../../shared/services/heroes.service';
import Swal from 'sweetalert2';
import { Heroes } from '../../../../shared/models/heroes';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent implements OnInit {

  @Output() headers = {
    "title": "Heroes",
    "bread": [
      {
        "title": "Tablero",
        "href": "dashboard"
      },
      {
        "title": "Heroes",
      }
    ]
  };
  @Output() modulo = "heroes";
  private sidebar_open: boolean;
  public Toast;
  public heroes: Heroes[];
  public preloader = true;


  constructor(private renderer: Renderer2, private servicio: HeroesService) {
    this.sidebar_open = document.body.classList.contains('sidebar-open');
    this.renderer.addClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');

    if (this.sidebar_open) {
      this.renderer.addClass(document.body, 'sidebar-closed');
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    this.ocultarPreloader();
  }

  ngOnInit(): void {
    this.listado();
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');
  }

  private ocultarPreloader () {
    setTimeout(() => {
      this.preloader = false;
    }, 1000);
  }

  private listado () {
    this.preloader = true;
    this.servicio.getHeroes()
      .subscribe(result => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        this.heroes = result.body.heroes;
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

  public eliminarHeroe (codigo: string) {
    this.preloader = true;
    let datos = new FormData();
    datos.append('_method', 'DELETE');

    this.servicio.deleteHeroe(codigo, datos)
      .subscribe(result => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        this.listado();
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

}
