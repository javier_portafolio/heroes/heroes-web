import { Component, OnInit, Output, Renderer2 } from '@angular/core';
import { HeroesService } from '../../../../shared/services/heroes.service';
import Swal from 'sweetalert2';
import { Heroes } from '../../../../shared/models/heroes';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-heroes-detail',
  templateUrl: './heroes-detail.component.html',
  styleUrls: ['./heroes-detail.component.css']
})
export class HeroesDetailComponent implements OnInit {

  @Output() headers = {
    "title": "Heroes",
    "bread": [
      {
        "title": "Tablero",
        "href": "dashboard"
      },
      {
        "title": "Heroes",
        "href": "heroes"
      },
      {
        "title": "Detalle"
      }
    ]
  };
  @Output() modulo = "heroes";
  private sidebar_open: boolean;
  public Toast;
  public codigo: string;
  public heroe: Heroes;
  public preloader = true;

  constructor(private renderer: Renderer2, private actRoute: ActivatedRoute, private servicio: HeroesService, private router: Router) {
    this.sidebar_open = document.body.classList.contains('sidebar-open');
    this.renderer.addClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');

    if (this.sidebar_open) {
      this.renderer.addClass(document.body, 'sidebar-closed');
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }

    this.Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 4000,
    });

    this.codigo = this.actRoute.snapshot.params.codigo;

    this.ocultarPreloader();
  }

  ngOnInit(): void {
    this.heroeDetalle();
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'sidebar-mini');
    this.renderer.addClass(document.body, 'layout-fixed');
  }

  private ocultarPreloader () {
    setTimeout(() => {
      this.preloader = false;
    }, 1000);
  }

  private heroeDetalle () {
    this.preloader = true
    this.servicio.getHeroe(this.codigo)
      .subscribe(result => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        this.heroe = result.body.heroe;
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

  public eliminarHeroe (codigo: string) {
    this.preloader = true;
    let datos = new FormData();
    datos.append('_method', 'DELETE');

    this.servicio.deleteHeroe(codigo, datos)
      .subscribe(result => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'success',
          title: result.message,
        });
        setTimeout(() => {
          this.router.navigateByUrl('/heroes');
        }, 2000);
      },
      err => {
        this.ocultarPreloader();
        this.Toast.fire({
          icon: 'error',
          title: err.error.message,
        });
      })
  }

}
