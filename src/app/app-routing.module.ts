import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationComponent } from './components/pages/auth/confirmation/confirmation.component';
import { ForgetComponent } from './components/pages/auth/forget/forget.component';
import { LoginComponent } from './components/pages/auth/login/login.component';
import { RecoveryComponent } from './components/pages/auth/recovery/recovery.component';
import { RegisterComponent } from './components/pages/auth/register/register.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { NotFoundComponent } from './components/pages/error/not-found/not-found.component';
import { HeroesCreateComponent } from './components/pages/heroes/heroes-create/heroes-create.component';
import { HeroesDetailComponent } from './components/pages/heroes/heroes-detail/heroes-detail.component';
import { HeroesEditComponent } from './components/pages/heroes/heroes-edit/heroes-edit.component';
import { HeroesListComponent } from './components/pages/heroes/heroes-list/heroes-list.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forget', component: ForgetComponent },
  { path: 'recovery', component: RecoveryComponent },
  { path: 'confirmation', component: ConfirmationComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'heroes', canActivate: [AuthGuard], children: [
    { path: '', component: HeroesListComponent },
    { path: 'create', component: HeroesCreateComponent },
    { path: ':codigo', component: HeroesDetailComponent },
    { path: ':codigo/edit', component: HeroesEditComponent }
  ] },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
